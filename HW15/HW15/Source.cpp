#include <iostream>

int N = 100;

// First version
void Numbers_printer(bool isEven_num, int N) //use isEven_num = true for "even" 
{

	for (int i = 0; i < N; i += 2)
	{
		std::cout << i + isEven_num*2 << ',';
	}
}

// Second version
void Numbers_printer_withZero(bool isOdd_num, int N) //use isOdd_num = false for "even" 
{

	for (int i = 0; i < N+1; i ++)
	{
		if (i % 2 == isOdd_num)
		{
			std::cout << i << ',';
		}
	}
}

int main()
{
	/*for (int i = 0; i < N;i+=2) 
	{
		std::cout << i<<',';
	}*/

	std::cout << "Without ZERO" << std::endl;
	Numbers_printer(true , 12);

	std::cout <<"\nWith ZERO"<<std::endl;

	Numbers_printer_withZero(false, 12);

	return 0;
}